package com.cs.chefvenderapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.cs.chefvenderapp.Fragments.ProfileScreen;
import com.cs.chefvenderapp.Fragments.OrderStatus;
import com.cs.chefvenderapp.Fragments.Renvenu;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int ACCOUNT_INTENT = 1;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;

    public static BottomNavigationView navigation;
    String format;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressLint("ResourceType")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    Fragment mainFragment = new OrderStatus();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

                    item.setIcon(R.drawable.home_icon);
                    Menu menu1 = navigation.getMenu();


                    return true;

//                case R.id.navigation_cart:
//
//                    item.setIcon(R.drawable.check_out);
//                    Fragment renvenuFragment = new Renvenu();
//                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, renvenuFragment).commit();
//
//                    Menu menu3 = navigation.getMenu();
//
//                    return true;
                case R.id.navigation_more:

                    item.setIcon(R.drawable.account);

                        Fragment accountFragment = new ProfileScreen();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();

                    return true;
            }
            return false;
        }
    };

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_arabic);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Menu menu = navigation.getMenu();

        if (language.equalsIgnoreCase("En")){
            MenuItem menuItem = menu.getItem(0);
            navigation.setSelectedItemId(menuItem.getItemId());
        } else {
            MenuItem menuItem = menu.getItem(1);
            navigation.setSelectedItemId(menuItem.getItemId());
        }

//            Fragment mainFragment = new OrderStatus();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCOUNT_INTENT && resultCode == RESULT_OK) {
//            Fragment accountFragment = new AccountFragment();
//            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
//            ft.commit();
        } else if (resultCode == RESULT_CANCELED) {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // Method for disabling ShiftMode of BottomNavigationView
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }


    @SuppressLint("ResourceType")
    @Override
    protected void onResume() {
        super.onResume();

        Menu menu = navigation.getMenu();

    }

}

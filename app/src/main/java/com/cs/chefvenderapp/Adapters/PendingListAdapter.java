package com.cs.chefvenderapp.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chefvenderapp.Activities.DriverListActivity;
import com.cs.chefvenderapp.Activities.LiveTrackingActivity;
import com.cs.chefvenderapp.Activities.OrderDetailsActivity;
import com.cs.chefvenderapp.Constants;
import com.cs.chefvenderapp.Models.OrderTypelist;
import com.cs.chefvenderapp.Models.UpdateOrderList;
import com.cs.chefvenderapp.NetworkUtil;
import com.cs.chefvenderapp.R;
import com.cs.chefvenderapp.Rest.APIInterface;
import com.cs.chefvenderapp.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.chefvenderapp.Activities.OrderTypeActivity.assigndriver;
import static com.cs.chefvenderapp.Activities.OrderTypeActivity.header_title;
import static com.cs.chefvenderapp.Activities.OrderTypeActivity.parameter;
import static com.cs.chefvenderapp.Constants.showOneButtonAlertDialog;

public class PendingListAdapter extends RecyclerView.Adapter<PendingListAdapter.MyViewHolder> {

    Context context;
    ArrayList<OrderTypelist.Data> orderLists = new ArrayList<>();
    //    ArrayList<OrderTypelist.AllAdminOrders> allAdminOrders = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid, language;
    public static String mcomment = "", order_id = "", orderstatus = "", customer_name = "", customer_email = "", morder_id = "";
    SharedPreferences userPrefs;
    int pos = -1;

    public PendingListAdapter(Context context, Activity activity, ArrayList<OrderTypelist.Data> orderLists, String userid, String language) {
        this.context = context;
        this.orderLists = orderLists;
        this.userid = userid;
        this.language = language;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_type_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_type_list_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        SimpleDateFormat current_date = new SimpleDateFormat("dd MMMM ", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);


        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        customer_email = userPrefs.getString("email", "");

        Log.i("TAG", "getView: " + userid);


        holder.name.setText("" + orderLists.get(position).getCustomerName());
        holder.mobile.setText("" + orderLists.get(position).getMobile());

        Date expect_date = null;
        String expected_date, expected_time;

        try {
            expect_date = dateFormat.parse(orderLists.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        expected_date = current_date.format(expect_date);
        expected_time = current_time.format(expect_date);

        holder.date.setText("" + expected_date);
        holder.time.setText("" + expected_time);

        holder.cash_online.setText("" + orderLists.get(position).getPaymentMode_En());
        holder.price.setText("" + Constants.decimalFormat.format(orderLists.get(position).getTotalPrice()));

        if (orderLists.get(position).getIsAssignDriver() == 0) {

            holder.driver_details.setText("Assign Driver");
            assigndriver = 0;

        } else {

            assigndriver = 1;
            holder.driver_details.setText("Driver Details");

        }

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = activity.getLayoutInflater();
                int layout = 0;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.comment_alert_dialog;
                } else if (language.equalsIgnoreCase("Ar")) {
                    layout = R.layout.comment_alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                TextView done = (TextView) dialogView.findViewById(R.id.done);


                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog1 = customDialog;
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finalCustomDialog1.dismiss();

                    }
                });

                final AlertDialog finalCustomDialog = customDialog;
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (comment.getText().toString().equals("")) {

                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog("Please enter reason", context.getResources().getString(R.string.app_name),
                                        context.getResources().getString(R.string.ok), activity);
                            } else {
                                showOneButtonAlertDialog("الرجاء كتابة السبب", context.getResources().getString(R.string.app_name_ar),
                                        context.getResources().getString(R.string.ok_ar), activity);
                            }

                        } else {

                            order_id = String.valueOf(orderLists.get(position).getOrderId());
                            mcomment = comment.getText().toString();
                            orderstatus = "Cancel";
                            customer_name = orderLists.get(position).getCustomerName();
                            new UpdateOrderApi().execute();
                            Intent intent = new Intent("UpdateOrder");
                            // You can also include some extra data.
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            finalCustomDialog.dismiss();

                        }

                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            }
        });

        holder.accept.setVisibility(View.VISIBLE);

        if (orderLists.get(position).getOrderStatus().equals("New")) {
            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.VISIBLE);
            holder.driver_details.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")) {
                holder.accept.setText("Accept");
            } else {
                holder.accept.setText("مقبول ");
            }
        } else if (orderLists.get(position).getOrderStatus().equals("Accepted")) {
            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.VISIBLE);
            holder.driver_details.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")) {
                holder.accept.setText("Ready");
            } else {
                holder.accept.setText("جاهز");
            }
        } else if (orderLists.get(position).getOrderStatus().equals("Ready")) {
            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.GONE);
            holder.driver_details.setVisibility(View.GONE);
            if (orderLists.get(position).getOrderMode_En().equals("Dine In")) {
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Served");
                } else {
                    holder.accept.setText("تم تقديمه");
                }
            }
            if (orderLists.get(position).getOrderMode_En().equals("Take Away/CarryOut")) {
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Served");
                } else {
                    holder.accept.setText("تم تقديمه");
                }
            }
            if (orderLists.get(position).getOrderMode_En().equals("Delivery")) {
                if (orderLists.get(position).getIsAssignDriver() == 0) {

                    holder.accept.setVisibility(View.GONE);

                } else {

                    holder.accept.setVisibility(View.VISIBLE);

                }
                holder.driver_details.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Locate");
                } else {
                    holder.accept.setText("Locate");
                }
            }
        } else if (orderLists.get(position).getOrderStatus().equals("DriverAccept")) {

            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.GONE);
            holder.driver_details.setVisibility(View.GONE);
            if (orderLists.get(position).getOrderMode_En().equals("Dine In")) {
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Served");
                } else {
                    holder.accept.setText("تم تقديمه");
                }
            }
            if (orderLists.get(position).getOrderMode_En().equals("Take Away/CarryOut")) {
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Served");
                } else {
                    holder.accept.setText("تم تقديمه");
                }
            }
            if (orderLists.get(position).getOrderMode_En().equals("Delivery")) {
                if (orderLists.get(position).getIsAssignDriver() == 0) {

                    holder.accept.setVisibility(View.GONE);

                } else {

                    holder.accept.setVisibility(View.VISIBLE);

                }
                holder.driver_details.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Locate");
                } else {
                    holder.accept.setText("Locate");
                }
            }

        } else if (orderLists.get(position).getOrderStatus().equals("OnTheWay")) {

            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.GONE);
            holder.driver_details.setVisibility(View.GONE);
            if (orderLists.get(position).getOrderMode_En().equals("Dine In")) {
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Served");
                } else {
                    holder.accept.setText("تم تقديمه");
                }
            }
            if (orderLists.get(position).getOrderMode_En().equals("Take Away/CarryOut")) {
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Served");
                } else {
                    holder.accept.setText("تم تقديمه");
                }
            }
            if (orderLists.get(position).getOrderMode_En().equals("Delivery")) {
                if (orderLists.get(position).getIsAssignDriver() == 0) {

                    holder.accept.setVisibility(View.GONE);

                } else {

                    holder.accept.setVisibility(View.VISIBLE);

                }
                holder.driver_details.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    holder.accept.setText("Locate");
                } else {
                    holder.accept.setText("Locate");
                }
            }

        } else if (orderLists.get(position).getOrderStatus().equals("Close")) {
            holder.accept.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);
            holder.driver_details.setVisibility(View.GONE);
        } else if (orderLists.get(position).getOrderStatus().equals("Cancel")) {
            holder.accept.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);
            holder.driver_details.setVisibility(View.GONE);
        }

        holder.driver_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);

                String date1;
                Date date = null;

                try {
                    date = dateFormat.parse(orderLists.get(position).getExpectedTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                date1 = dateFormat1.format(date);

                Intent a = new Intent(context, DriverListActivity.class);
                a.putExtra("Brandid", orderLists.get(position).getBrandId());
                a.putExtra("Branchid", orderLists.get(position).getBranchId());
                a.putExtra("Orderid", orderLists.get(position).getOrderId());
                a.putExtra("exp_time", date1);
                a.putExtra("orderstatus", orderLists.get(position).getOrderStatus());
                context.startActivity(a);

            }
        });

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.accept.getText().toString().equals("Locate")) {

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);

                    String date1;
                    Date date = null;

                    try {
                        date = dateFormat.parse(orderLists.get(position).getExpectedTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    date1 = dateFormat1.format(date);

                    Intent a = new Intent(context, LiveTrackingActivity.class);
                    a.putExtra("order_id", orderLists.get(position).getOrderId());
                    a.putExtra("exp_time", date1);
                    a.putExtra("orderstatus", orderLists.get(position).getOrderStatus());
                    context.startActivity(a);


                } else {
                    pos = position;
                    mcomment = "";
                    order_id = String.valueOf(orderLists.get(position).getOrderId());
                    if (orderLists.get(position).getOrderStatus().equals("New")) {
                        orderstatus = "Accepted";
                    } else if (orderLists.get(position).getOrderStatus().equals("Accepted")) {
                        orderstatus = "Ready";
                    } else if (orderLists.get(position).getOrderStatus().equals("Ready")) {

                        orderstatus = "Delivered";
                    }
                    customer_name = orderLists.get(position).getCustomerName();
                    Log.i("TAG", "onBindViewHolder: " + orderLists.get(position).getOrderStatus());
                    Intent intent = new Intent("UpdateOrder");
                    // You can also include some extra data.
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    new UpdateOrderApi().execute();
                }

            }
        });

        if (language.equalsIgnoreCase("En")) {

            holder.no_of_orders.setText(" " + orderLists.get(position).getQuantity() + " items");
            String item_name_qty = null;
            if (orderLists.get(position).getOrderItems() != null) {
                for (int i = 0; i < orderLists.get(position).getOrderItems().size(); i++) {

                    if (orderLists.get(position).getOrderItems().size() == 1) {

                        item_name_qty = orderLists.get(position).getOrderItems().get(i).getNameEn() + " X " + orderLists.get(position).getOrderItems().get(i).getItemQuantity() + ".";

                    } else {

                        if (i == 0) {
                            item_name_qty = orderLists.get(position).getOrderItems().get(i).getNameEn() + " X " + orderLists.get(position).getOrderItems().get(i).getItemQuantity();
                        } else if (i == (orderLists.get(position).getOrderItems().size() - 1)) {
                            item_name_qty = item_name_qty + ", " + orderLists.get(position).getOrderItems().get(i).getNameEn() + " X " + orderLists.get(position).getOrderItems().get(i).getItemQuantity() + ".";
                        } else {
                            item_name_qty = item_name_qty + ", " + orderLists.get(position).getOrderItems().get(i).getNameEn() + " X " + orderLists.get(position).getOrderItems().get(i).getItemQuantity();
                        }

                    }
                }
                holder.item_name_qty.setText("" + item_name_qty);

            } else {

                holder.item_name_qty.setVisibility(View.INVISIBLE);

            }

        } else {

            holder.no_of_orders.setText(" " + orderLists.get(position).getQuantity() + "عناصر ");
            String item_name_qty = null;
            if (orderLists.get(position).getOrderItems() != null) {
                for (int i = 0; i < orderLists.get(position).getOrderItems().size(); i++) {

                    if (orderLists.get(position).getOrderItems().size() == 1) {

                        item_name_qty = orderLists.get(position).getOrderItems().get(i).getItemQuantity() + " X " + orderLists.get(position).getOrderItems().get(i).getNameAr() + ".";

                    } else {

                        if (i == 0) {
                            item_name_qty = orderLists.get(position).getOrderItems().get(i).getItemQuantity() + " X " + orderLists.get(position).getOrderItems().get(i).getNameAr();
                        } else if (i < orderLists.get(position).getOrderItems().size()) {
                            item_name_qty = item_name_qty + ", " + orderLists.get(position).getOrderItems().get(i).getItemQuantity() + " X " + orderLists.get(position).getOrderItems().get(i).getNameAr() + ".";
                        } else {
                            item_name_qty = item_name_qty + ", " + orderLists.get(position).getOrderItems().get(i).getItemQuantity() + " X " + orderLists.get(position).getOrderItems().get(i).getNameAr();
                        }

                    }
                }

                holder.item_name_qty.setText("" + item_name_qty);

            } else {

                holder.item_name_qty.setVisibility(View.INVISIBLE);

            }
        }

        morder_id = String.valueOf(orderLists.get(position).getOrderId());

    }

    @Override
    public int getItemCount() {

        return orderLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, mobile, time, date, no_of_orders, item_name_qty, cash_online, price, reject, accept, driver_details;

        public MyViewHolder(View convertView) {
            super(convertView);

            name = (TextView) convertView.findViewById(R.id.name);
            mobile = (TextView) convertView.findViewById(R.id.mobile_number);
            time = (TextView) convertView.findViewById(R.id.time);
            date = (TextView) convertView.findViewById(R.id.date);
            no_of_orders = (TextView) convertView.findViewById(R.id.total_orders);
            item_name_qty = (TextView) convertView.findViewById(R.id.item_names_qty);
            cash_online = (TextView) convertView.findViewById(R.id.cash_online);
            price = (TextView) convertView.findViewById(R.id.price);
            reject = (TextView) convertView.findViewById(R.id.reject);
            accept = (TextView) convertView.findViewById(R.id.accept);
            driver_details = (TextView) convertView.findViewById(R.id.driver_details);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, OrderDetailsActivity.class);
                    a.putExtra("header", header_title);
                    a.putExtra("order_id", orderLists.get(getAdapterPosition()).getOrderId());
                    a.putExtra("parameter", parameter);
                    a.putExtra("assigndriver", orderLists.get(getAdapterPosition()).getIsAssignDriver());
                    a.putExtra("vendorcharge", orderLists.get(getAdapterPosition()).getVendorDeliverycost());
                    a.putExtra("totalcharge", orderLists.get(getAdapterPosition()).getTotalDeliverycost());
                    a.putExtra("drivershare", orderLists.get(getAdapterPosition()).getDriverDeliveryShare());
                    a.putExtra("chefshare", orderLists.get(getAdapterPosition()).getChefDeliveryShare());
                    a.putExtra("driverpers", orderLists.get(getAdapterPosition()).getDriverSharePercent());
                    a.putExtra("chefpers", orderLists.get(getAdapterPosition()).getChefSharePercent());
                    Log.i("TAG", "onClick: " + orderLists.get(getAdapterPosition()).getOrderId());
                    context.startActivity(a);

                }
            });

        }

    }

    private String prepareJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("orderId", order_id);
            parentObj.put("OrderStatus", orderstatus);
            parentObj.put("CancelReason", mcomment);
            parentObj.put("customerName", customer_name);
            parentObj.put("CustomerEmail", customer_email);
            parentObj.put("AdminUserId", userid);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareChangePasswordJson: " + parentObj);

        return parentObj.toString();
    }

    private class UpdateOrderApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
//            dialog = new ACProgressFlower.Builder(context)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(activity);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UpdateOrderList> call = apiService.getupdateorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateOrderList>() {
                @Override
                public void onResponse(Call<UpdateOrderList> call, Response<UpdateOrderList> response) {
                    if (response.isSuccessful()) {
                        UpdateOrderList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

//                            if (mcomment.equalsIgnoreCase("")) {
//
//                                Constants.showOneButtonAlertDialog("Request Successful", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);
//
//                            } else {
//
//                                Constants.showOneButtonAlertDialog("Order Rejected Successfully", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);
//
//                            }
                            if (pos != -1) {
                                if (orderLists.get(pos).getOrderStatus().equals("Accepted")) {

                                    if (!orderLists.get(pos).getOrderMode_En().equals("Take Away/CarryOut")) {

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);

                                        String date1;
                                        Date date = null;

                                        try {
                                            date = dateFormat.parse(orderLists.get(pos).getExpectedTime());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        date1 = dateFormat1.format(date);

                                        Intent a = new Intent(context, DriverListActivity.class);
                                        a.putExtra("Brandid", orderLists.get(pos).getBrandId());
                                        a.putExtra("Branchid", orderLists.get(pos).getBranchId());
                                        a.putExtra("Orderid", orderLists.get(pos).getOrderId());
                                        a.putExtra("exp_time", date1);
                                        a.putExtra("orderstatus", orderLists.get(pos).getOrderStatus());
                                        context.startActivity(a);

                                    }

                                }
                            }

                            notifyDataSetChanged();

                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = updateorderList.getMessage();
                                showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                        context.getResources().getString(R.string.ok), activity);
                            } else {
                                showOneButtonAlertDialog(updateorderList.getMessageAr(), context.getResources().getString(R.string.app_name_ar),
                                        context.getResources().getString(R.string.ok_ar), activity);

                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateOrderList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

}
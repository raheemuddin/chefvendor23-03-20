package com.cs.chefvenderapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chefvenderapp.Constants;
import com.cs.chefvenderapp.Models.OrderDetailsList;
import com.cs.chefvenderapp.R;

import java.util.ArrayList;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.MyViewHolder> {

    Context context;
    Activity activity;
    LayoutInflater inflater;
    String userid, language;
    ArrayList<OrderDetailsList.OrderItems> orderItems = new ArrayList<>();
    OrderDetailsAdditionalsAdapter orderDetailsAdditionalsAdapter;

    public OrderDetailsAdapter(Context context, ArrayList<OrderDetailsList.OrderItems> orderItems , String language) {
        this.context = context;
        this.userid = userid;
        this.language = language;
        this.orderItems = orderItems;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
             itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_details_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_details_list_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.item_name_qty.setText("" + orderItems.get(position).getItemName_En() + " X " + "" + orderItems.get(position).getQuantity());
            holder.item_price.setText("" + Constants.decimalFormat.format(orderItems.get(position).getItemPrice()) + " SAR");
//            holder.notes.setText("Notes : No notes");
        } else {
            holder.item_name_qty.setText("" + orderItems.get(position).getQuantity() + " X " + "" +  orderItems.get(position).getItemName_Ar());
            holder.item_price.setText("" + Constants.decimalFormat.format(orderItems.get(position).getItemPrice()) + " SAR");
//            holder.notes.setText("Notes : No notes");
        }

        Glide.with(context).load(Constants.ITEM_IMAGE_URL + orderItems.get(position).getItemImage()).into(holder.item_img);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(context);
        holder.additional_list.setLayoutManager(layoutManager1);
        if (orderItems.get(position).getAdditionalItems().size() == 0){
            holder.additional_layout.setVisibility(View.GONE);
        }else {
            holder.additional_layout.setVisibility(View.VISIBLE);
            orderDetailsAdditionalsAdapter = new OrderDetailsAdditionalsAdapter(context, orderItems.get(position).getAdditionalItems(), language);
            holder.additional_list.setAdapter(orderDetailsAdditionalsAdapter);
        }

    }

    @Override
    public int getItemCount() {
//        if (userid.equalsIgnoreCase("1")) {
//            return allAdminOrders.size();
//        } else {
//            return orderLists.size();
//        }
        return orderItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item_name_qty, item_price, notes;
        ImageView item_img;
        LinearLayout additional_layout;
        RecyclerView additional_list;

        public MyViewHolder(final View convertView) {
            super(convertView);

            item_name_qty = (TextView) convertView.findViewById(R.id.item_name_qty);
            item_price = (TextView) convertView.findViewById(R.id.item_price);
            notes = (TextView) convertView.findViewById(R.id.note);
            item_img = (ImageView) convertView.findViewById(R.id.item_img);
            additional_layout = (LinearLayout) convertView.findViewById(R.id.additional_layout);
            additional_list = (RecyclerView) convertView.findViewById(R.id.additional_list);


//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }

    }

}

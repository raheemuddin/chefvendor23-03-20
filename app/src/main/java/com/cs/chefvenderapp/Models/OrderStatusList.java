package com.cs.chefvenderapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderStatusList {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("OrdersCounts")
        private ArrayList<OrdersCounts> OrdersCounts;
        @Expose
        @SerializedName("BranchesDetails")
        private ArrayList<BranchesDetails> BranchesDetails;
        @Expose
        @SerializedName("UserDetails")
        private ArrayList<UserDetails> UserDetails;

        public ArrayList<OrdersCounts> getOrdersCounts() {
            return OrdersCounts;
        }

        public void setOrdersCounts(ArrayList<OrdersCounts> OrdersCounts) {
            this.OrdersCounts = OrdersCounts;
        }

        public ArrayList<BranchesDetails> getBranchesDetails() {
            return BranchesDetails;
        }

        public void setBranchesDetails(ArrayList<BranchesDetails> BranchesDetails) {
            this.BranchesDetails = BranchesDetails;
        }

        public ArrayList<UserDetails> getUserDetails() {
            return UserDetails;
        }

        public void setUserDetails(ArrayList<UserDetails> UserDetails) {
            this.UserDetails = UserDetails;
        }
    }

    public static class OrdersCounts {
        @Expose
        @SerializedName("ScheduledOrders")
        private int ScheduledOrders;
        @Expose
        @SerializedName("CancelOrders")
        private int CancelOrders;
        @Expose
        @SerializedName("ClosedOrders")
        private int ClosedOrders;
        @Expose
        @SerializedName("ReadyOrders")
        private int ReadyOrders;
        @Expose
        @SerializedName("AcceptOrders")
        private int AcceptOrders;
        @Expose
        @SerializedName("NewOrders")
        private int NewOrders;
        @Expose
        @SerializedName("TotalOrders")
        private int TotalOrders;
        @Expose
        @SerializedName("TotalRevenue")
        private double TotalRevenue;

        public int getScheduledOrders() {
            return ScheduledOrders;
        }

        public void setScheduledOrders(int ScheduledOrders) {
            this.ScheduledOrders = ScheduledOrders;
        }

        public int getCancelOrders() {
            return CancelOrders;
        }

        public void setCancelOrders(int CancelOrders) {
            this.CancelOrders = CancelOrders;
        }

        public int getClosedOrders() {
            return ClosedOrders;
        }

        public void setClosedOrders(int ClosedOrders) {
            this.ClosedOrders = ClosedOrders;
        }

        public int getReadyOrders() {
            return ReadyOrders;
        }

        public void setReadyOrders(int ReadyOrders) {
            this.ReadyOrders = ReadyOrders;
        }

        public int getAcceptOrders() {
            return AcceptOrders;
        }

        public void setAcceptOrders(int AcceptOrders) {
            this.AcceptOrders = AcceptOrders;
        }

        public int getNewOrders() {
            return NewOrders;
        }

        public void setNewOrders(int NewOrders) {
            this.NewOrders = NewOrders;
        }

        public int getTotalOrders() {
            return TotalOrders;
        }

        public void setTotalOrders(int TotalOrders) {
            this.TotalOrders = TotalOrders;
        }

        public double getTotalRevenue() {
            return TotalRevenue;
        }

        public void setTotalRevenue(double TotalRevenue) {
            this.TotalRevenue = TotalRevenue;
        }
    }

    public static class BranchesDetails {
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("AdminUserId")
        private int AdminUserId;
        @Expose
        @SerializedName("ShopNo")
        private String ShopNo;
        @Expose
        @SerializedName("BranchCode")
        private String BranchCode;
        @Expose
        @SerializedName("Name_ar")
        private String Name_ar;
        @Expose
        @SerializedName("Name_en")
        private String Name_en;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public int getAdminUserId() {
            return AdminUserId;
        }

        public void setAdminUserId(int AdminUserId) {
            this.AdminUserId = AdminUserId;
        }

        public String getShopNo() {
            return ShopNo;
        }

        public void setShopNo(String ShopNo) {
            this.ShopNo = ShopNo;
        }

        public String getBranchCode() {
            return BranchCode;
        }

        public void setBranchCode(String BranchCode) {
            this.BranchCode = BranchCode;
        }

        public String getName_ar() {
            return Name_ar;
        }

        public void setName_ar(String Name_ar) {
            this.Name_ar = Name_ar;
        }

        public String getName_en() {
            return Name_en;
        }

        public void setName_en(String Name_en) {
            this.Name_en = Name_en;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }

    public static class UserDetails {
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("userType")
        private int userType;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("firstName")
        private String firstName;
        @Expose
        @SerializedName("userId")
        private int userId;

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getUserType() {
            return userType;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }
}

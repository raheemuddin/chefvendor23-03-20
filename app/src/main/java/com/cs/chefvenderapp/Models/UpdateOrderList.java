package com.cs.chefvenderapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateOrderList {


    @Expose
    @SerializedName("Data")
    private String Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public String getData() {
        return Data;
    }

    public void setData(String Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}

package com.cs.chefvenderapp.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chefvenderapp.Adapters.PendingListAdapter;
import com.cs.chefvenderapp.Constants;
import com.cs.chefvenderapp.Models.OrderTypelist;
import com.cs.chefvenderapp.NetworkUtil;
import com.cs.chefvenderapp.R;
import com.cs.chefvenderapp.Rest.APIInterface;
import com.cs.chefvenderapp.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderTypeActivity extends AppCompatActivity {

    //    LinearLayout pending;
    LinearLayout scheduled, order;
    //    TextView pending_txt;
    TextView scheduled_txt, order_txt;
    //    View pending_view;
    View scheduled_view, order_view;
    String language, userId = "1";
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    //    boolean pending_boolean = false;
    boolean scheduled_boolean = false, order_boolean = false;
    ImageView back_btn;

    public static Activity fa;
    ArrayList<OrderTypelist.Data> orderLists = new ArrayList<>();
    ArrayList<OrderTypelist.OrderItems> itemsLists = new ArrayList<>();

    //    ArrayList<OrderTypelist.Data> dine_inorderLists = new ArrayList<>();
    ArrayList<OrderTypelist.Data> pickuporderLists = new ArrayList<>();
    ArrayList<OrderTypelist.Data> deliveryorderLists = new ArrayList<>();

    RecyclerView mpending_list;
    PendingListAdapter pendingListAdapter;
    public static String header, header_title, parameter;
    TextView header_txt;
    public static int assigndriver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_types);
        } else {
            setContentView(R.layout.order_types_arabic);
        }
//        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        fa = this;

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        header_txt = findViewById(R.id.header);
        back_btn = findViewById(R.id.back_btn);

        header_txt.setText("" + getIntent().getStringExtra("header"));
        header = getIntent().getStringExtra("header");
        parameter = getIntent().getStringExtra("parameter");
        header_title = getIntent().getStringExtra("header");

//        pending = findViewById(R.id.pending);
        scheduled = findViewById(R.id.schedule);
        order = findViewById(R.id.order);

//        pending_txt = findViewById(R.id.pending_txt);
        scheduled_txt = findViewById(R.id.schedule_txt);
        order_txt = findViewById(R.id.order_txt);

//        pending_view = findViewById(R.id.pending_view);
        scheduled_view = findViewById(R.id.schedule_view);
        order_view = findViewById(R.id.order_view);

        mpending_list = findViewById(R.id.pendinglist);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

//        pending.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                pending_boolean = true;
//                scheduled_boolean = false;
//                order_boolean = false;
//                pending_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
//                order_txt.setTextColor(getResources().getColor(R.color.black));
//
//                pending_view.setVisibility(View.VISIBLE);
//                scheduled_view.setVisibility(View.GONE);
//                order_view.setVisibility(View.GONE);
//
//                new Orderstatusapi().execute();
//
//            }
//        });

        scheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                pending_boolean = false;
                scheduled_boolean = true;
                order_boolean = false;

//                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                order_txt.setTextColor(getResources().getColor(R.color.black));

//                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.VISIBLE);
                order_view.setVisibility(View.GONE);

                new Orderstatusapi().execute();

            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                pending_boolean = false;
                scheduled_boolean = false;
                order_boolean = true;

//                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                order_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

//                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.VISIBLE);

                new Orderstatusapi().execute();

            }
        });


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderTypeActivity.this);
        mpending_list.setLayoutManager(mLayoutManager);

        new Orderstatusapi().execute();

        LocalBroadcastManager.getInstance(OrderTypeActivity.this).registerReceiver(
                mupdate_order, new IntentFilter("UpdateOrder"));
    }

    private BroadcastReceiver mupdate_order = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("TAG", "onBroadcastReceive: UpdateOrder");

            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
            mpending_list.setAdapter(pendingListAdapter);

//                        if (pending_boolean) {
//
//                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId);
//                            mpending_list.setAdapter(pendingListAdapter);
//
//                        } else

            if (scheduled_boolean) {

                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                mpending_list.setAdapter(pendingListAdapter);

            } else if (order_boolean) {

                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
                mpending_list.setAdapter(pendingListAdapter);

            }

            new Orderstatusapi1().execute();

        }
    };

    private String prepareChangePasswordJson() {
        JSONObject parentObj = new JSONObject();

        parameter = parameter.replace(" Orders", "");

        Log.i("TAG", "header: " + header);

        if (parameter.equalsIgnoreCase("Closed")) {
            parameter = "Close";
        }
        if (parameter.equalsIgnoreCase("Schedule")) {
            parameter = "New";
        }

        try {
            parentObj.put("UserId", userId);
            parentObj.put("OrderStatus", parameter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareChangePasswordJson: " + parentObj);

        return parentObj.toString();
    }

    private class Orderstatusapi extends AsyncTask<String, Integer, String> {

//        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
//            dialog = new ACProgressFlower.Builder(OrderTypeActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            if (!((Activity) OrderTypeActivity.this).isFinishing()) {
//
//                dialog.show();
//            }
            Constants.showLoadingDialog(OrderTypeActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderTypelist> call = apiService.orderstatus(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderTypelist>() {
                @Override
                public void onResponse(Call<OrderTypelist> call, Response<OrderTypelist> response) {

                    Log.i("TAG", "onResponse: " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        OrderTypelist Response = response.body();

                        orderLists = Response.getData();

//                        dine_inorderLists.clear();
                        pickuporderLists.clear();
                        deliveryorderLists.clear();
                        Log.i("TAG", "count: " + orderLists.size());

                        for (int i = 0; i < orderLists.size(); i++) {
//                            if (orderLists.get(i).getOrderMode_En().equals("Dine In")) {
//                                dine_inorderLists.add(orderLists.get(i));
//                            }
                            Log.i("TAG", "Order type: " + orderLists.get(i).getOrderMode_En());

                            if (orderLists.get(i).getOrderMode_En().equals("Take Away/CarryOut")) {
                                pickuporderLists.add(orderLists.get(i));
                            }
                            if (orderLists.get(i).getOrderMode_En().equals("Delivery")) {
                                deliveryorderLists.add(orderLists.get(i));
                            }
                        }

//                        pending_txt.setText("Dine_In (" + dine_inorderLists.size() + ")");

                        if (language.equalsIgnoreCase("En")) {

                            scheduled_txt.setText("Pickup (" + pickuporderLists.size() + ")");

                            order_txt.setText("Delivery (" + deliveryorderLists.size() + ")");

                        } else {

                            scheduled_txt.setText("(" + pickuporderLists.size() + ") طريقة التوصيل");

                            order_txt.setText("(" + deliveryorderLists.size() + ") استلام");

                        }


                        pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                        mpending_list.setAdapter(pendingListAdapter);

//                        if (pending_boolean) {
//
//                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId);
//                            mpending_list.setAdapter(pendingListAdapter);
//
//                        } else

                        if (scheduled_boolean) {

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                        } else if (order_boolean) {

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                        }

                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderTypelist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    private class Orderstatusapi1 extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderTypelist> call = apiService.orderstatus(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderTypelist>() {
                @Override
                public void onResponse(Call<OrderTypelist> call, Response<OrderTypelist> response) {

                    Log.i("TAG", "onResponse: " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        OrderTypelist Response = response.body();

                        orderLists = Response.getData();

//                        dine_inorderLists.clear();
                        pickuporderLists.clear();
                        deliveryorderLists.clear();
                        Log.i("TAG", "count: " + orderLists.size());

                        for (int i = 0; i < orderLists.size(); i++) {
//                            if (orderLists.get(i).getOrderMode_En().equals("Dine In")) {
//                                dine_inorderLists.add(orderLists.get(i));
//                            }
                            Log.i("TAG", "Order type: " + orderLists.get(i).getOrderMode_En());

                            if (orderLists.get(i).getOrderMode_En().equals("Take Away/CarryOut")) {
                                pickuporderLists.add(orderLists.get(i));
                            }
                            if (orderLists.get(i).getOrderMode_En().equals("Delivery")) {
                                deliveryorderLists.add(orderLists.get(i));
                            }
                        }

//                        pending_txt.setText("Dine_In (" + dine_inorderLists.size() + ")");

                        if (language.equalsIgnoreCase("En")) {

                            scheduled_txt.setText("Pickup (" + pickuporderLists.size() + ")");

                            order_txt.setText("Delivery (" + deliveryorderLists.size() + ")");

                        } else {

                            scheduled_txt.setText("(" + pickuporderLists.size() + ") طريقة التوصيل");

                            order_txt.setText("(" + deliveryorderLists.size() + ") استلام");

                        }


                        pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                        mpending_list.setAdapter(pendingListAdapter);

//                        if (pending_boolean) {
//
//                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId);
//                            mpending_list.setAdapter(pendingListAdapter);
//
//                        } else

                        if (scheduled_boolean) {

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                        } else if (order_boolean) {

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                        }

                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<OrderTypelist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                }
            });
            return null;
        }
    }

}

package com.cs.chefvenderapp.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chefvenderapp.Adapters.OrderDetailsAdapter;
import com.cs.chefvenderapp.Constants;
import com.cs.chefvenderapp.Models.OrderDetailsList;
import com.cs.chefvenderapp.Models.UpdateOrderList;
import com.cs.chefvenderapp.NetworkUtil;
import com.cs.chefvenderapp.R;
import com.cs.chefvenderapp.Rest.APIInterface;
import com.cs.chefvenderapp.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.chefvenderapp.Activities.OrderTypeActivity.assigndriver;
import static com.cs.chefvenderapp.Constants.showOneButtonAlertDialog;

public class OrderDetailsActivity extends AppCompatActivity {

    TextView order_details_total_price, service_type, item_qty, reject, accept, driver_details, delivery_date_time;
    TextView payment_mode, total_amount, discount_amount, delivery_charger, vat, net_amount, day, month, user_name, address_txt, mobile_no;
    RecyclerView order_details_list;
    String mcomment = "", order_id = "", orderstatus = "", customer_name = "", customer_email = "";
    SharedPreferences userPrefs;
    TextView header_txt;
    OrderDetailsAdapter orderDetailsAdapter;
    ImageView back_btn;
    View view;
    String userId;
    int morder_id, assigndriver;
    ArrayList<OrderDetailsList.Data> orderDetailsLists = new ArrayList<>();
    TextView invoice_no, order_status;
    int pos = -1;

    SharedPreferences LanguagePrefs;
    String language;
    RelativeLayout vendor_delivery_layout, deliver_charge_layout;
    View view4;
    LinearLayout admin_delivery_layout;
    double str_vendor_charge, str_totalcharge, str_drivershare, str_chefshare, str_driverpers, str_chefpers;
    TextView vendor_delivery_charge, customer_charge, vendor_charge, total_charge, driver_share_txt, driver_share, chef_share_txt, chef_share;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_details);
        } else {
            setContentView(R.layout.order_details_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        customer_email = userPrefs.getString("email", "");
        userId = userPrefs.getString("userId", "0");

        header_txt = findViewById(R.id.header);
        back_btn = findViewById(R.id.back_btn);

        header_txt.setText("" + getIntent().getStringExtra("header"));
        morder_id = getIntent().getIntExtra("order_id", 0);
        assigndriver = getIntent().getIntExtra("assigndriver", 0);
        str_vendor_charge = getIntent().getDoubleExtra("vendorcharge", 0);
        str_totalcharge = getIntent().getDoubleExtra("totalcharge", 0);
        str_drivershare = getIntent().getDoubleExtra("drivershare", 0);
        str_chefshare = getIntent().getDoubleExtra("chefshare", 0);
        str_driverpers = getIntent().getDoubleExtra("driverpers", 0);
        str_chefpers = getIntent().getDoubleExtra("chefpers", 0);

        order_details_total_price = findViewById(R.id.order_details_total_amount);
        service_type = findViewById(R.id.service_type);
        item_qty = findViewById(R.id.item_qty);
        reject = findViewById(R.id.reject);
        accept = findViewById(R.id.accept);
        driver_details = findViewById(R.id.driver_details);
        delivery_date_time = findViewById(R.id.delivery_date_time);
        payment_mode = findViewById(R.id.payment_option);
        total_amount = findViewById(R.id.total_amount);
        discount_amount = findViewById(R.id.discount_amount);
        delivery_charger = findViewById(R.id.delivery_charger);
        vat = findViewById(R.id.vat);
        net_amount = findViewById(R.id.net_amount);
//        note = findViewById(R.id.note);
        day = findViewById(R.id.day);
        month = findViewById(R.id.month);
        user_name = findViewById(R.id.user_name);
        address_txt = findViewById(R.id.address_txt);
        mobile_no = findViewById(R.id.mobile_number);
        order_details_list = findViewById(R.id.order_details_list);
        view = findViewById(R.id.view1);

        invoice_no = findViewById(R.id.invoice_no);
        order_status = findViewById(R.id.order_status);

        view4 = (View) findViewById(R.id.view4);

        vendor_delivery_layout = (RelativeLayout) findViewById(R.id.vendor_delivery_layout);
        deliver_charge_layout = (RelativeLayout) findViewById(R.id.delivery_charger_layout);

        admin_delivery_layout = (LinearLayout) findViewById(R.id.admin_delivery_layout);

        customer_charge = (TextView) findViewById(R.id.customer_charge);
        vendor_delivery_charge = (TextView) findViewById(R.id.vendor_delivery_charges);
        vendor_charge = (TextView) findViewById(R.id.vendor_charge);
        total_charge = (TextView) findViewById(R.id.total_delivery_fee);
        driver_share_txt = (TextView) findViewById(R.id.driver_share_txt);
        driver_share = (TextView) findViewById(R.id.driver_share);
        chef_share_txt = (TextView) findViewById(R.id.chef_share_txt);
        chef_share = (TextView) findViewById(R.id.chef_share);


        if (assigndriver == 0) {

            driver_details.setText("Assign Driver");

        } else {

            driver_details.setText("Driver Details");

        }

        accept.setVisibility(View.VISIBLE);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("TAG", "onBackPressed1: ");
                OrderTypeActivity.fa.finish();
                Intent a = new Intent(OrderDetailsActivity.this, OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", getIntent().getStringExtra("header"));
                } else {
                    a.putExtra("header", getIntent().getStringExtra("header"));
                }
                a.putExtra("parameter", getIntent().getStringExtra("parameter"));
                startActivity(a);
                finish();

            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this);
        order_details_list.setLayoutManager(mLayoutManager);

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetailsActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = 0;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.comment_alert_dialog;
                } else if (language.equalsIgnoreCase("Ar")) {
                    layout = R.layout.comment_alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                TextView done = (TextView) dialogView.findViewById(R.id.done);

                customDialog = dialogBuilder.create();
                customDialog.show();


                final AlertDialog finalCustomDialog1 = customDialog;
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finalCustomDialog1.dismiss();

                    }
                });

                final AlertDialog finalCustomDialog = customDialog;
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (comment.getText().toString().equals("")) {

                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog("Please enter reason", getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), OrderDetailsActivity.this);
                            } else {
                                showOneButtonAlertDialog("الرجاء كتابة السبب", getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);

                            }

                        } else {

                            for (int i = 0; i < orderDetailsLists.size(); i++) {
                                order_id = String.valueOf(orderDetailsLists.get(i).getOrderMain().getOrderId());
                                mcomment = comment.getText().toString();
                                orderstatus = "Cancel";
                                customer_name = orderDetailsLists.get(i).getOrderMain().getUserName();
                            }

                            new UpdateOrderApi().execute();

                            finalCustomDialog.dismiss();

                        }

                    }
                });


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            }
        });

        driver_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);

                String date1;
                Date date = null;

                try {
                    date = dateFormat.parse(orderDetailsLists.get(0).getOrderMain().getExpectedTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                date1 = dateFormat1.format(date);

                Intent a = new Intent(OrderDetailsActivity.this, DriverListActivity.class);
                a.putExtra("Brandid", orderDetailsLists.get(0).getOrderMain().getBrandId());
                a.putExtra("Branchid", orderDetailsLists.get(0).getOrderMain().getBranchId());
                a.putExtra("Orderid", orderDetailsLists.get(0).getOrderMain().getOrderId());
                a.putExtra("exp_time", date1);
                a.putExtra("orderstatus", orderDetailsLists.get(0).getOrderMain().getOrderStatus());
                startActivity(a);

            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (accept.getText().toString().equals("Locate")) {

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);

                    String date1;
                    Date date = null;

                    try {
                        date = dateFormat.parse(orderDetailsLists.get(0).getOrderMain().getExpectedTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    date1 = dateFormat1.format(date);

                    Intent a = new Intent(OrderDetailsActivity.this, LiveTrackingActivity.class);
                    a.putExtra("order_id", orderDetailsLists.get(0).getOrderMain().getOrderId());
                    a.putExtra("exp_time", date1);
                    a.putExtra("orderstatus", orderDetailsLists.get(0).getOrderMain().getOrderStatus());
                    startActivity(a);


                } else {

                    mcomment = "";
                    for (int i = 0; i < orderDetailsLists.size(); i++) {
                        order_id = String.valueOf(orderDetailsLists.get(i).getOrderMain().getOrderId());
                        if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("New")) {
                            orderstatus = "Accepted";
                        } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Accepted")) {
                            orderstatus = "Ready";
                        } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Ready")) {
                            orderstatus = "Delivered";
                        }
                        customer_name = orderDetailsLists.get(i).getOrderMain().getUserName();
                    }
                    new UpdateOrderApi().execute();

                }
            }
        });

        new OrderdetailsApi().execute();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.i("TAG", "onBackPressed: ");
        OrderTypeActivity.fa.finish();
        Intent a = new Intent(OrderDetailsActivity.this, OrderTypeActivity.class);
        if (language.equalsIgnoreCase("En")) {
            a.putExtra("header", getIntent().getStringExtra("header"));
        } else {
            a.putExtra("header", getIntent().getStringExtra("header"));
        }
        a.putExtra("parameter", getIntent().getStringExtra("parameter"));
        startActivity(a);
        finish();

    }

    private String prepareJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("orderId", morder_id);
            parentObj.put("OrderStatus", orderstatus);
            parentObj.put("CancelReason", mcomment);
            parentObj.put("customerName", customer_name);
            parentObj.put("CustomerEmail", customer_email);
            parentObj.put("AdminUserId", userId);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }

    private class UpdateOrderApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
//            dialog = new ACProgressFlower.Builder(OrderDetailsActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(OrderDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UpdateOrderList> call = apiService.getupdateorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateOrderList>() {
                @Override
                public void onResponse(Call<UpdateOrderList> call, Response<UpdateOrderList> response) {
                    if (response.isSuccessful()) {
                        UpdateOrderList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

//                            if (mcomment.equalsIgnoreCase("")) {
//
//                                Constants.showOneButtonAlertDialog("Request Successful", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OrderDetailsActivity.this);
//
//                            } else {
//
//                                Constants.showOneButtonAlertDialog("Order Rejected Successfully", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OrderDetailsActivity.this);
//
//                            }


                            if (orderDetailsLists.get(0).getOrderMain().getOrderStatus().equals("Accepted")) {

                                if (!orderDetailsLists.get(0).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {

                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);

                                    String date1;
                                    Date date = null;

                                    try {
                                        date = dateFormat.parse(orderDetailsLists.get(0).getOrderMain().getExpectedTime());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    date1 = dateFormat1.format(date);

                                    Intent a = new Intent(OrderDetailsActivity.this, DriverListActivity.class);
                                    a.putExtra("Brandid", orderDetailsLists.get(0).getOrderMain().getBrandId());
                                    a.putExtra("Branchid", orderDetailsLists.get(0).getOrderMain().getBranchId());
                                    a.putExtra("Orderid", orderDetailsLists.get(0).getOrderMain().getOrderId());
                                    a.putExtra("exp_time", date1);
                                    a.putExtra("orderstatus", orderDetailsLists.get(0).getOrderMain().getOrderStatus());
                                    startActivity(a);
                                    finish();

                                }

                            } else {

                                OrderTypeActivity.fa.finish();
                                Intent a = new Intent(OrderDetailsActivity.this, OrderTypeActivity.class);
                                if (language.equalsIgnoreCase("En")) {
                                    a.putExtra("header", getIntent().getStringExtra("header"));
                                } else {
                                    a.putExtra("header", getIntent().getStringExtra("header"));
                                }
                                a.putExtra("parameter", getIntent().getStringExtra("parameter"));
                                startActivity(a);
                                finish();

                            }

                        } else {
                            String failureResponse = updateorderList.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), OrderDetailsActivity.this);
                            } else {
                                showOneButtonAlertDialog(updateorderList.getMessageAr(), getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateOrderList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareOrderDetailsJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("OrderId", morder_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "orderid: " + morder_id);
        Log.i("TAG", "prepareorderdetailsJson: " + parentObj);

        return parentObj.toString();
    }

    private class OrderdetailsApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareOrderDetailsJson();
//            dialog = new ACProgressFlower.Builder(OrderDetailsActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(OrderDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderDetailsList> call = apiService.getorderdetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderDetailsList>() {
                @Override
                public void onResponse(Call<OrderDetailsList> call, Response<OrderDetailsList> response) {
                    if (response.isSuccessful()) {
                        OrderDetailsList orderdetailsResponse = response.body();
                        if (orderdetailsResponse.getStatus()) {

                            orderDetailsLists = orderdetailsResponse.getData();

                            for (int i = 0; i < orderDetailsLists.size(); i++) {

                                invoice_no.setText("" + orderDetailsLists.get(i).getOrderMain().getInvoiceNo());
                                order_status.setText("" + orderDetailsLists.get(i).getOrderMain().getOrderStatus());

                                orderDetailsAdapter = new OrderDetailsAdapter(OrderDetailsActivity.this, orderDetailsLists.get(i).getOrderItems(), language);
                                order_details_list.setAdapter(orderDetailsAdapter);

                                if (language.equalsIgnoreCase("En")) {
                                    order_details_total_price.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " SAR");
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
                                        service_type.setText("Pickup");
                                        view4.setVisibility(View.GONE);
                                        deliver_charge_layout.setVisibility(View.GONE);
                                    } else {
                                        view4.setVisibility(View.VISIBLE);
                                        deliver_charge_layout.setVisibility(View.VISIBLE);
                                        vendor_delivery_charge.setText("" + Constants.decimalFormat.format(str_vendor_charge) + " SAR");
                                        vendor_charge.setText("+ " + Constants.decimalFormat.format(str_vendor_charge) + " SAR");
                                        customer_charge.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getDeliveryCharges()) + " SAR");
                                        total_charge.setText("" + Constants.decimalFormat.format(str_totalcharge) + " SAR");
                                        driver_share.setText("" + Constants.decimalFormat.format(str_drivershare) + " SAR");
                                        chef_share.setText("" + Constants.decimalFormat.format(str_chefshare) + " SAR");
                                        driver_share_txt.setText("Driver Share (" + str_driverpers + "%)");
                                        chef_share_txt.setText("Chef Share (" + str_chefpers + "%)");

                                        if (userId.equalsIgnoreCase("1")) {
                                            admin_delivery_layout.setVisibility(View.VISIBLE);
                                            vendor_delivery_layout.setVisibility(View.GONE);
                                        } else {
                                            admin_delivery_layout.setVisibility(View.GONE);
                                            vendor_delivery_layout.setVisibility(View.VISIBLE);
                                        }

                                        service_type.setText("" + orderDetailsLists.get(i).getOrderMain().getOrderMode_En());
                                    }
                                } else {
                                    order_details_total_price.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " SAR");
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
                                        service_type.setText("طريقة التوصيل");
                                        view4.setVisibility(View.GONE);
                                        deliver_charge_layout.setVisibility(View.GONE);
                                    } else {
                                        view4.setVisibility(View.VISIBLE);
                                        deliver_charge_layout.setVisibility(View.VISIBLE);
                                        vendor_delivery_charge.setText("" + Constants.decimalFormat.format(str_vendor_charge) + " SAR");
                                        vendor_charge.setText("+ " + Constants.decimalFormat.format(str_vendor_charge) + " SAR");
                                        customer_charge.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getDeliveryCharges()) + " SAR");
                                        total_charge.setText("" + Constants.decimalFormat.format(str_totalcharge) + " SAR");
                                        driver_share.setText("" + Constants.decimalFormat.format(str_drivershare) + " SAR");
                                        chef_share.setText("" + Constants.decimalFormat.format(str_chefshare) + " SAR");
                                        driver_share_txt.setText("Driver Share (" + str_driverpers + "%)");
                                        chef_share_txt.setText("Chef Share (" + str_chefpers + "%)");

                                        if (userId.equalsIgnoreCase("1")) {
                                            admin_delivery_layout.setVisibility(View.VISIBLE);
                                            vendor_delivery_layout.setVisibility(View.GONE);
                                        } else {
                                            admin_delivery_layout.setVisibility(View.GONE);
                                            vendor_delivery_layout.setVisibility(View.VISIBLE);
                                        }
                                        service_type.setText("" + orderDetailsLists.get(i).getOrderMain().getOrderMode_Ar());
                                    }
                                }
                                item_qty.setText("" + orderDetailsLists.get(i).getOrderMain().getTotalItemsQty());

                                final int finalI1 = i;


                                if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("New")) {
                                    accept.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.VISIBLE);
                                    driver_details.setVisibility(View.GONE);
                                    if (language.equalsIgnoreCase("En")) {
                                        accept.setText("Accept");
                                    } else {
                                        accept.setText("تم قبوله");
                                    }
                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Accepted")) {
                                    accept.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.VISIBLE);
                                    driver_details.setVisibility(View.GONE);
                                    if (language.equalsIgnoreCase("En")) {
                                        accept.setText("Ready");
                                    } else {
                                        accept.setText("جاهز");
                                    }
                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Ready")) {
                                    accept.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.GONE);
                                    driver_details.setVisibility(View.GONE);
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Dine In")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Served");
                                        } else {
                                            accept.setText("يقدم");
                                        }
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Served");
                                        } else {
                                            accept.setText("يقدم");
                                        }
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Delivery")) {

                                        if (assigndriver == 0) {

                                            accept.setVisibility(View.GONE);

                                        } else {

                                            accept.setVisibility(View.VISIBLE);

                                        }
                                        driver_details.setVisibility(View.VISIBLE);
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Locate");
                                        } else {
                                            accept.setText("Locate");
                                        }

//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Pickup");
//                                        } else {
//                                            accept.setText("طريقة التوصيل");
//                                        }
                                    }
                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("DriverAccept")) {

                                    accept.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.GONE);
                                    driver_details.setVisibility(View.GONE);
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Dine In")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Served");
                                        } else {
                                            accept.setText("تم تقديمه");
                                        }
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Served");
                                        } else {
                                            accept.setText("تم تقديمه");
                                        }
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Delivery")) {
                                        if (assigndriver == 0) {

                                            accept.setVisibility(View.GONE);

                                        } else {

                                            accept.setVisibility(View.VISIBLE);

                                        }
                                        driver_details.setVisibility(View.VISIBLE);
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Locate");
                                        } else {
                                            accept.setText("Locate");
                                        }
                                    }

                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("OnTheWay")) {

                                    accept.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.GONE);
                                    driver_details.setVisibility(View.GONE);
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Dine In")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Served");
                                        } else {
                                            accept.setText("تم تقديمه");
                                        }
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Served");
                                        } else {
                                            accept.setText("تم تقديمه");
                                        }
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Delivery")) {
                                        if (assigndriver == 0) {

                                            accept.setVisibility(View.GONE);

                                        } else {

                                            accept.setVisibility(View.VISIBLE);

                                        }
                                        driver_details.setVisibility(View.VISIBLE);
                                        if (language.equalsIgnoreCase("En")) {
                                            accept.setText("Locate");
                                        } else {
                                            accept.setText("Locate");
                                        }
                                    }

                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Close")) {
                                    accept.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    view.setVisibility(View.GONE);
                                    driver_details.setVisibility(View.GONE);
                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Cancel")) {
                                    accept.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    view.setVisibility(View.GONE);
                                    driver_details.setVisibility(View.GONE);
                                }

                                final int finalI = i;

                                SimpleDateFormat current_date = new SimpleDateFormat("dd MMM yy hh:mm aa", Locale.US);
                                SimpleDateFormat dayFormat = new SimpleDateFormat("dd", Locale.US);
                                SimpleDateFormat mouthFormat = new SimpleDateFormat("MMMM", Locale.US);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

                                Date delivery_date = null;
                                String delivery_date1;

                                try {
                                    delivery_date = dateFormat.parse(orderDetailsLists.get(i).getOrderMain().getOrderDate());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                delivery_date1 = current_date.format(delivery_date);

                                delivery_date_time.setText(delivery_date1);
                                if (language.equalsIgnoreCase("En")) {

                                    payment_mode.setText("" + orderDetailsLists.get(i).getOrderMain().getPaymentMode());
                                    total_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " SAR");
                                    if (orderDetailsLists.get(i).getOrderMain().getCouponDiscount() == 0) {
                                        discount_amount.setText("- " + "0.00" + " SAR");
                                    } else {
                                        discount_amount.setText("- " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getCouponDiscount()) + " SAR");
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getDeliveryCharges() == 0) {
                                        delivery_charger.setText("+ " + "0.00" + " SAR");
                                    } else {
                                        delivery_charger.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getDeliveryCharges()) + " SAR");
                                    }
                                    vat.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getVatCharges()) + " SAR");
                                    net_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " SAR");

                                } else {

                                    payment_mode.setText("" + orderDetailsLists.get(i).getOrderMain().getPaymentMode());
                                    total_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " SAR");
                                    if (orderDetailsLists.get(i).getOrderMain().getCouponDiscount() == 0) {
                                        discount_amount.setText("- " + "0.00" + " SAR");
                                    } else {
                                        discount_amount.setText("- " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getCouponDiscount()) + " SAR");
                                    }
                                    if (orderDetailsLists.get(i).getOrderMain().getDeliveryCharges() == 0) {
                                        delivery_charger.setText("+ " + "0.00" + " SAR");
                                    } else {
                                        delivery_charger.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getDeliveryCharges()) + " SAR");
                                    }
                                    vat.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getVatCharges()) + " SAR");
                                    net_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " SAR");

                                }
//                                note.setText("Note:");
                                //order date
                                Date day1 = null;
                                String day2, month2;

                                try {
                                    day1 = dateFormat.parse(orderDetailsLists.get(i).getOrderMain().getOrderDate());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                day2 = dayFormat.format(day1);
                                month2 = mouthFormat.format(day1);

                                day.setText(day2);

                                month.setText(month2);
                                user_name.setText("" + orderDetailsLists.get(i).getOrderMain().getUserName());
                                if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Delivery")) {
                                    address_txt.setText("" + orderDetailsLists.get(i).getOrderMain().getUserAddress());
                                    mobile_no.setText("" + orderDetailsLists.get(i).getOrderMain().getMobileNo());
                                } else {
                                    address_txt.setText("" + orderDetailsLists.get(i).getOrderMain().getBranchAddress());
                                    mobile_no.setText("" + orderDetailsLists.get(i).getOrderMain().getBranchMobileNo());
                                }

                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderDetailsList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

}
